public class Rollup_Methods_TriggerHandler {

    public static void rollupStatementsToRentalAgreement(List<Statement__c> newList){
        
        //First we create a Set of Ids of Rental Agreements
        Set<Id> rentalAgreementSet = new Set<Id>();
        
        //We looping through the Statements List and adding the Rental Agreements to the Set
        for(Statement__c s : newList){
            rentalAgreementSet.add(s.Rental_Agreement__c);
        }
        
        //Than we create a List to store the Rental Agreements records wich needs to be updated
        List<Rental_Agreement__c> rentalUpdates = new List<Rental_Agreement__c>();
        
        //Aggregate Query to Sum Total Amount for Statements
        for(AggregateResult ar : [SELECT sum(Total_Amount__c) totalInvoice, Rental_Agreement__c FROM Statement__c WHERE Rental_Agreement__c IN:rentalAgreementSet GROUP BY Rental_Agreement__c]){
            
           //Instantiate and fetching with Rental Agreement object
           Rental_Agreement__c r = new Rental_Agreement__c(Id = String.valueOf(ar.get('Rental_Agreement__c')));
           r.Total_Invoiced__c = double.valueOf(ar.get('totalInvoice'));
           rentalUpdates.add(r);
        }
        
        if(!rentalUpdates.isEmpty()){
            update rentalUpdates;
        }
    }
    
    public static void rollupPaymentsToRentalAgreement (Map<Id, Payment__c> newMap){
        
        //Create a Set of Ids of Rental Agreements
        Set<Id> rentalAgreementSet = new Set<Id>();
        
        //Looping through the Payment Map and adding to the Id Set
        for(Payment__c p : [SELECT Id, Statement__r.Rental_Agreement__c FROM Payment__c WHERE Id IN :newMap.keySet()]){
            rentalAgreementSet.add(p.Statement__r.Rental_Agreement__c);
        }
        
        //Than we create a List to store the Rental Agreements records wich needs to be updated
        List<Rental_Agreement__c> rentalUpdates = new List<Rental_Agreement__c>();
        
        for(AggregateResult ar : [SELECT sum(Amount__c) totalPaid, Statement__r.Rental_Agreement__c rentalAgr FROM Payment__c WHERE Statement__r.Rental_Agreement__c IN: rentalAgreementSet GROUP BY Statement__r.Rental_Agreement__c]){
            
            Rental_Agreement__c r = new Rental_Agreement__c(Id = String.valueOf(ar.get('rentalAgr')));
            r.Total_Payments__c = double.valueOf(ar.get('totalPaid'));
            rentalUpdates.add(r);
        }
        
        if(!rentalUpdates.isEmpty()){
            update rentalUpdates;
        }
    }
}