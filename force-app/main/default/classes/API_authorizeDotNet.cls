public class API_authorizeDotNet {

    // Create variables to hold our login credentials
    public static String APILOGIN;
    public static String APITRANSKEY;
    public static String ENDPOINT;
    
    //Retrive the values from Custom Setting "Authorize_Net_Setting__c"
    public static void getAuthNetCred(){
        Authorize_Net_Setting__c apiLoginSetting = Authorize_Net_Setting__c.getInstance('API Login');
        Authorize_Net_Setting__c apiTransKeySetting = Authorize_Net_Setting__c.getInstance('TransKey');
        Authorize_Net_Setting__c endpointSetting = Authorize_Net_Setting__c.getInstance('EndpointURL');
            
        APILOGIN = apiLoginSetting.Value__c;
        APITRANSKEY = apiTransKeySetting.Value__c;
        ENDPOINT = endpointSetting.Value__c;
        
        System.debug('APILOGIN ' + APILOGIN); 
        System.debug('APITRANSKEY ' + APITRANSKEY);
        System.debug('ENDPOINT ' + ENDPOINT);
    }
    
    public static authNetResp_wrapper authDotNetCharge(authNetReq_wrapper input){
        
        // Reusable Method for getting Authorize.net credentials
        // Send our credentials first
        getAuthNetCred();
        
        //Construct our request
        //We gonna send a request from Salesforce to the Payment Gateway, that is Authorization.net
        HttpRequest req = new HttpRequest();
        
        // Endpoint URL, defined in Custom Setting, we use test endpoint URL
        // For testing use the Test endpoint
        // Otherwise use 'https://secure.authorize.net/gateway/transact.dll'
        req.setEndpoint(ENDPOINT);
        
        System.debug('APILOGIN ' + APILOGIN); 
        System.debug('APITRANSKEY ' + APITRANSKEY);
        System.debug('ENDPOINT ' + ENDPOINT);
        
        // We are submitted the request, so we are using POST method
        req.setMethod('POST');
        
        //Build the message
        Map<String, String> messageString = new Map<String, String>();
        
        //Default Fields
        //For more information see the authorization.net documentation
        messageString.put('x_login', APILOGIN); // x_login came from the authorization.net (see documentation)
        messageString.put('x_tran_key', APITRANSKEY);
        messageString.put('x_version', '3.1');
        messageString.put('x_delim_data', 'TRUE');
        messageString.put('x_delim_char', ';');
        messageString.put('x_relay_response', 'FALSE');
        
        //The type of the transaction
        messageString.put('x_type', 'AUTH_CAPTURE');
        messageString.put('x_method', 'CC');
        
        //Transaction specific information
        messageString.put('x_card_num', input.ccnum);
        messageString.put('x_exp_date', input.ccexp);
        messageString.put('x_card_code', input.ccsec);
        
        //Transaction Amount
        messageString.put('x_amount', input.amt);
        
        //Description of transaction
        messageString.put('x_description', 'Your Transaction ' + input.ordername);
        
        //Billing Information
       	messageString.put('x_first_name', input.firstname);
        messageString.put('x_last_name', input.lastname);
        messageString.put('x_address', input.billstreet);
        messageString.put('x_city', input.billcity);
        messageString.put('x_state', input.billstate);
        messageString.put('x_zip', input.billzip);
        
        //COnverting Map to the String
        //Encode the message components
        String encodedMsg = '';
        
        //Loop through the messageString Map
        for(String s: messageString.keySet()){
            String v = messageString.get(s); // for the each key we retrive the value
            //Fix null values
            if(String.isBlank(v)){
                v = '';  
            }else{
                encodedMsg += s + '=' + EncodingUtil.urlDecode(v, 'UTF-8')+'&';
            }
        }
        
        //Add message termination
        encodedMsg += 'endofdata';
        System.debug('encodedMsg' + encodedMsg);
        
        //Add the encodedMsg to the request body
       	req.setBody(encodedMsg);
        
        //Create object of the Http class. Send and collect the response
        Http http = new Http();
        String resp = http.send(req).getBody(); // response from payment gateway
        
        System.debug('Response from Authorize.net ' + resp);
        
        //Split the response by our delimiter, defined in messageString map 'x_delim_char' ;
        List <String> responses = resp.split(';'); 
        
        //We gonna parse our response into the wrapper
        authNetResp_wrapper parsedResponse = parseIntoResponseWrapper(responses);
        
        return parsedResponse;
    }
    
    // The return type of this method will be authNetResp_wrapper
    // Response which comes back from paynment gateway, we convert into a list using the delimeter character.
    // And now we are converting that list into authNetResponse_wrapper
    
    public static authNetResp_wrapper parseIntoResponseWrapper(List <String> input){ // takes parameter of the type List<String>, and comes from "responses"
        //Create temporary variable
        authNetResp_wrapper temp = new authNetResp_wrapper();
        temp.responseCode = input[0];
        temp.responseSubCode = input[1];
        temp.responseReasonCode = input[2];
        temp.responseReasonText = input[3]; 
        temp.authorizationCode = input[4]; 
        temp.AVSResponse = input[5]; 
        temp.transactionID = input[6]; 
        temp.invoiceNumber = input[7]; 
        temp.description = input[8];
        temp.amount = input[9]; 
        temp.method = input[10]; 
        temp.transactionType = input[11]; 
        temp.customerID = input[12]; 
        temp.firstName = input[13]; 
        temp.lastName = input[14]; 
        temp.company = input[15]; 
        temp.address = input[16]; 
        temp.city = input[17]; 
        temp.state = input[18]; 
        temp.ZIPcode = input[19]; 
        temp.country = input[20]; 
        temp.phone = input[21]; 
        temp.fax = input[22]; 
        temp.emailAddress = input[23]; 
        temp.shipToFirstName = input[24];
        temp.shipToLastName = input[25]; 
        temp.shipToCompany = input[26];
        temp.shipToAddress = input[27];
        temp.shipToCity = input[28];
        temp.shipToState = input[29];
        temp.shipToZipCode = input[30];
        temp.shipToCountry = input[31];
        temp.tax = input[32];
        temp.duty = input[33];
        temp.freight = input[34];
        temp.taxExempt = input[35];
        temp.purchaseOrderNumber = input[36];
        temp.MD5Hash = input[37];
        temp.cardCodeResponse = input[38];
        temp.cardHolderAuthenticationVerificationResponse = input[39];
        temp.accountNumber = input[40];
        temp.cardType = input[41];
        temp.splitTenderID = input[42];
        temp.requestedAmount = input[43];
        temp.balanceOnCard = input[44];
        
        return temp;
    }
    
    public class authNetReq_wrapper{
        public String ordername {get;set;}
        public String ccnum {get;set;}
        public String ccexp {get;set;}
        public String ccsec {get;set;}
        public String amt {get;set;}
        public String firstname {get;set;}
        public String lastname {get;set;}
        public String billstreet {get;set;}
        public String billcity {get;set;}
        public String billstate {get;set;}
        public String billzip {get;set;}
        public String transid {get;set;}
        public String routingnumber {get;set;}
        public String accountnumber {get;set;}
        public String bankaccounttype {get;set;}
        public String bankname {get;set;}
        public String bankaccountname {get;set;}
        
        //Empty constructor 
        public authNetReq_wrapper(){
            
        }
    }
    
    // Create 42 variables, all go from the Authorize.net documentation, and then we gonna convert this to the response object.
    // this is a purpose of the wrapper
    
    public class authNetResp_wrapper{
        public String responseCode {get;set;}
        public String responseSubCode {get;set;}
        public String responseReasonCode {get;set;}
        public String responseReasonText {get;set;} 
        public String authorizationCode {get;set;} 
        public String AVSResponse {get;set;} 
        public String transactionID {get;set;} 
        public String invoiceNumber {get;set;} 
        public String description {get;set;} 
        public String amount {get;set;} 
        public String method {get;set;} 
        public String transactionType {get;set;} 
        public String customerID {get;set;} 
        public String firstName {get;set;} 
        public String lastName {get;set;} 
        public String company {get;set;} 
        public String address {get;set;} 
        public String city {get;set;} 
        public String state {get;set;} 
        public String ZIPcode {get;set;} 
        public String country {get;set;} 
        public String phone {get;set;} 
        public String fax {get;set;} 
        public String emailAddress {get;set;} 
        public String shipToFirstName {get;set;} 
        public String shipToLastName {get;set;} 
        public String shipToCompany {get;set;}
        public String shipToAddress {get;set;} 
        public String shipToCity {get;set;}
        public String shipToState {get;set;}
        public String shipToZipCode {get;set;}
        public String shipToCountry {get;set;}
        public String tax {get;set;}
        public String duty {get;set;}
        public String freight {get;set;}
        public String taxExempt {get;set;}
        public String purchaseOrderNumber {get;set;}
        public String MD5Hash {get;set;}
        public String cardCodeResponse {get;set;}
        public String cardHolderAuthenticationVerificationResponse {get;set;}
        public String accountNumber {get;set;}
        public String cardType {get;set;}
        public String splitTenderID {get;set;}
        public String requestedAmount {get;set;}
        public String balanceOnCard {get;set;}
       
        //Empty constructor
        public authNetResp_wrapper(){
            
        }
    }
}