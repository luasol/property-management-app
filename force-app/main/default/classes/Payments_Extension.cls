public class Payments_Extension {
    
    //Variables
    //Payment__c record variable to hold payment information
    public Payment__c thisPayment {get;set;} 
    public Statement__c thisStatement {get;set;}
    public Map <Id,String> recordTypeMap {get;set;}
    
    //Boolean to dermine if payment was successful
    public Boolean success {get;set;}
    
    //Constructor declaration
    public Payments_Extension (ApexPages.StandardController scon){
        if (scon.getRecord()instanceOf Statement__c){
            thisStatement = [SELECT Id, Name, Rental_Agreement__r.Renter__c, Balance__c FROM Statement__c WHERE Id=:scon.getId()];
        }
        success = false;
        
        recordTypeMap = new Map <Id,String>();
        //Query for all the record types in payment object
        //and we will store it in the map, populate map
        for(RecordType r : [SELECT Id, Name FROM RecordType WHERE sObjectType = 'Payment__c']){
            recordTypeMap.put(r.id, r.Name);
        } 
        
        //Instantiate the Payment variable
        thisPayment = new Payment__c();
        if(scon.getRecord() instanceOf Statement__c){
            thisPayment.Statement__c = scon.getId();
            thisPayment.Amount__c = thisStatement.Balance__c;
        }
        
        //If guest User, preset type to Credit Card
        if(getIsGuest()){
            for(id i: recordTypeMap.keySet()){
                if(recordTypeMap.get(i)=='Credit Card'){
                    thisPayment.RecordTypeId = i;
                    break;
                }
            }
            
        }
        //If not Guest, populate billing details from renter
        else if(scon.getRecord() instanceOf Statement__c){
                    Contact renter = [SELECT Id, FirstName, LastName, mailingStreet, mailingCity, mailingState, mailingPostalCode FROM Contact WHERE Id=:thisStatement.Rental_Agreement__r.Renter__c];
                    thisPayment.Billing_Name__c = renter.FirstName + ' ' + renter.LastName;
                    thisPayment.Billing_Street__c = renter.MailingStreet;
                    thisPayment.Billing_City__c = renter.MailingCity;
                    thisPayment.Billing_State__c = renter.MailingState;
                    thisPayment.Billing_Postal_Code__c = renter.MailingPostalCode;
                    
       }
    }
    
    //This method will process and save our payment
    //or report any errors in the attempt
    public PageReference savePayment(){
        success = false;
        //Retrive the Payment Type
        String paymentType = recordTypeMap.get(thisPayment.RecordTypeId);
        //Validation
        if(validateFields(paymentType)){
            //Process Credit Card Payments
            if(paymentType == 'Credit Card'){
                //Create a request wrapper for Authorize.net
                //from class API_authorizeDotNet method fro request wrapper
                API_authorizeDotNet.authNetReq_wrapper req = new API_authorizeDotNet.authNetReq_wrapper();
                
                //Set the wrapper values
                req.amt = String.valueOf(thisPayment.Amount__c);
                
                // We check if thisPayment.Billing_Name__c contains space between, and if contains we put 
                // only what is before the space to firstname, if not we put everything to the firstname field. Using method String.subStringBefore(' ')
                // The same for the lastname, but using method String.subStringAfter(' ')
                req.firstname = (thisPayment.Billing_Name__c.contains(' ')) ? thisPayment.Billing_Name__c.subStringBefore(' ') : thisPayment.Billing_Name__c;
                req.lastname = (thisPayment.Billing_Name__c.contains(' ')) ? thisPayment.Billing_Name__c.subStringAfter(' ') : thisPayment.Billing_Name__c;
                req.billstreet = thisPayment.Billing_Street__c;
                req.billcity = thisPayment.Billing_City__c;
                req.billstate = thisPayment.Billing_State__c;
                req.billzip = thisPayment.Billing_Postal_Code__c;
                
                //Set up Credit Card information on the request wrapper
                req.ccnum = thisPayment.Credit_Card_Number__c;
                req.ccexp = monthmap.get(thisPayment.Credit_Card_Expiration_Month__c) + thisPayment.Expiration_Year__c;
                req.ccsec = thisPayment.Credit_Card_Security_Code__c;
                
                //Give this req a name
                req.ordername = 'Payment of ' + [SELECT Id, Name FROM Statement__c WHERE id=:thisPayment.Statement__c].name;
                
                //Process our Authorize.net request
                API_authorizeDotNet.authNetResp_wrapper res = API_authorizeDotNet.authDotNetCharge(req);
                thisPayment.Authorize_net_Transaction_Id__c = res.transactionID;
                thisPayment.Authorize_net_Authorization_code__c = res.authorizationCode;
                thisPayment.Authorize_net_Response__c = res.responseCode + '| ' + res.responseReasonText;
                
                //If the Transaction failed
                if(res.responseCode != '1' || res.responseReasonText != 'This Transaction has been approved.'){
                    thisPayment.Status__c = 'Failed';
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Payment Failed'));
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'res.responseReasonText'));
                    
                    return null;
                }
            }
            
            //If the Transaction successful
            thisPayment.Status__c = 'Paid';
            thisPayment.Payment_Date__c = System.now();
            upsert thisPayment;
            success = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, 'Payment Successful'));
                    
            try{
               //If there is an email provided by renter
               if(thisStatement != null && thisStatement.Rental_Agreement__r.Renter__r.Email != null){
                   //Construct email message
                   Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                   msg.setToAddresses(new List<String> {thisStatement.Rental_Agreement__r.Renter__r.Email});
                   msg.setSubject('Payment Confirmation');
                   msg.setHtmlBody('Your Payment of ' + thisPayment.Amount__c + 'has been successfully processed. <br/><br/> Thank You!');
                   msg.setPlainTextBody('Your Payment of ' + thisPayment.Amount__c + 'has been successfully processed. \n \n Thank You!');
                                
                   //Send email
                   Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {msg});
                }
            }catch(Exception e){}                       
                
        }else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please fill out all the details'));
        }
        return null;
    } 
    
    //Verify required fields have been filled out
    public Boolean validateFields(String paymentType){
        Boolean valid = true;
        
        //Check commons Fields
        if(thisPayment.Statement__c == null){
            valid = false;
        }
        if(thisPayment.Amount__c == null){
            valid = false;
        }
        if(String.isBlank(thisPayment.Billing_Name__c)){
            valid = false;
        }
        if(String.isBlank(thisPayment.Billing_Street__c)){
            valid = false;
        }
        if(String.isBlank(thisPayment.Billing_City__c)){
            valid = false;
        }
        if(String.isBlank(thisPayment.Billing_State__c)){
            valid = false;
        }
        if(String.isBlank(thisPayment.Billing_Postal_Code__c)){
            valid = false;
        }
        
        //Check specific fields related to the Payment Type
        //
        //Check fields when the Payment Type is 'Check' and 'Credit Card'
        if(paymentType == 'Check'){
            if(String.isBlank(thisPayment.Check_Account_Number__c)){
                valid = false;
            }
            if(String.isBlank(thisPayment.Check_Routing_Number__c)){
                valid = false;
            }
        }else if(paymentType == 'Credit Card'){
            if(String.isBlank(thisPayment.Credit_Card_Number__c)){
                valid = false;
            }
            if(String.isBlank(thisPayment.Credit_Card_Expiration_Month__c)){
                valid = false;
            }
            if(String.isBlank(thisPayment.Expiration_Year__c)){
                valid = false;
            }
            if(String.isBlank(thisPayment.Credit_Card_Security_Code__c)){
                valid = false;
            }
        }
        
        return valid;
    }
    
    public Boolean getIsGuest(){
        return [SELECT Id, userType FROM Profile WHERE id =: userInfo.getProfileId()].userType == 'Guest';
    }
    
     public List<SelectOption> getPaymentRecordTypes(){
        List<SelectOption> temp = new List<SelectOption>();
         
        //Select option structure is value, label, disabled (optional)
        temp.add(new SelectOption('', 'Select Payment Method'));
        for(id i:recordTypeMap.keySet()){
           temp.add(new SelectOption(i, recordTypeMap.get(i)));
        }
        return temp;
    }
    
    //This method will always show us 5 next years from actual year dynamically.
    public List<SelectOption> getExpirationYears(){
        List<SelectOption> temp = new List<SelectOption>();
        for(Integer i = 0; i < 5; i++){
            String y = '' + System.today().addYears(i).year();
            temp.add(new SelectOption(y, y));
        }
        return temp;
    }
    
    //Authorize.net expected two representing digits for each month, that is why we should assign the month to digits.
    public static Map<String,String> monthmap = new Map<String,String>{
        'January'=>'01',
            'February'=>'02',
            'March'=>'03',
            'April'=>'04',
            'May'=>'05',
            'June'=>'06',
            'July'=>'07',
            'August'=>'08',
            'September'=>'09',
            'October'=>'10',
            'November'=>'11',
            'December'=>'12'        
    };
                
}